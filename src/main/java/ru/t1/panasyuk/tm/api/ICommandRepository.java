package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
