package ru.t1.panasyuk.tm;

import ru.t1.panasyuk.tm.api.ICommandRepository;
import ru.t1.panasyuk.tm.constant.ArgumentConst;
import ru.t1.panasyuk.tm.constant.CommandConst;
import ru.t1.panasyuk.tm.model.Command;
import ru.t1.panasyuk.tm.repository.CommandRepository;
import ru.t1.panasyuk.tm.util.TerminalUtil;

import static ru.t1.panasyuk.tm.util.FormatUtil.formatBytes;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void processCommand(String command) {
        switch (command) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct ...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.err.println("Current command is not correct ...");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Petr Panasyuk");
        System.out.println("E-mail: ppanasyuk@t1-consulting.ru");
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");

        final int processorsCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSORS: " + processorsCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        for (Command command : COMMAND_REPOSITORY.getCommands()) {
            System.out.println(command);
        }
    }

}